packer {
    required_plugins {
        virtualbox = {
          version = "~> 1"
          source  = "github.com/hashicorp/virtualbox"
        }
        vagrant = {
          version = "~> 1"
          source = "github.com/hashicorp/vagrant"
           }
        
    }
}


source "virtualbox-iso" "basic-example" {
  guest_os_type = "Ubuntu_64"
  iso_url = "http://releases.ubuntu.com/jammy/ubuntu-22.04.3-live-server-amd64.iso"
  iso_checksum = "sha256:a4acfda10b18da50e2ec50ccaf860d7f20b389df8765611142305c0e911d16fd"
  boot_command          = [
                           "c",
                           "linux /casper/vmlinuz autoinstall ",
                           "ds='nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/' --- <enter><wait>",
                           "initrd /casper/initrd<enter><wait>",
                           "boot<enter>"
                          ]
  boot_wait             = "10s"
  communicator          = "ssh"
  vm_name               = "nsm-ubuntu" 
  cpus                  = "2"
  memory                = "2046"
  disk_size             = "81920"
  headless              = false
  http_directory        = "http"
  ssh_username          = "vagrant"
  ssh_password          = "vagrant"
  ssh_port              = "22"
  ssh_timeout           = "15m"
  shutdown_command      = "echo 'vagrant' | sudo -S shutdown -P now"

  vboxmanage = [
    ["modifyvm", "{{.Name}}", "--nat-localhostreachable1", "on"],
  ]

}

build {
  sources = ["sources.virtualbox-iso.basic-example"]

  provisioner "shell" {
      inline = [
        "export DEBIAN_FRONTEND=noninteractive",
        "apt install tree apache2 --yes",
        "apt install linux-headers-$(uname -r) --yes",
        "apt install bzip2 tar build-essential dkms --yes",
        "mkdir -p /home/vagrant/.ssh",
        "chmod 0700 /home/vagrant/.ssh",
        "curl https://raw.githubusercontent.com/hashicorp/vagrant/main/keys/vagrant.pub -o /home/vagrant/.ssh/authorized_keys",
        "chmod 0600 /home/vagrant/.ssh/authorized_keys",
        "wget http://download.virtualbox.org/virtualbox/7.0.14/VBoxGuestAdditions_7.0.14.iso -P /tmp",
        "mount -o loop -t iso9660 /tmp/VBoxGuestAdditions_7.0.14.iso /mnt",
        "sh /mnt/VBoxLinuxAdditions.run",
        "umount /mnt",
        "rm /tmp/VBoxGuestAdditions_7.0.14.iso"
        

      ]
      execute_command = "echo 'vagrant' | sudo -S sh -c '{{ .Vars }} {{ .Path }}'"

  }

    post-processor "vagrant" {
      keep_input_artifact = true
      provider_override   = "virtualbox"
    }
  }
