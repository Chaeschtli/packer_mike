#!/bin/bash

# Dieses Bash-Shell-Skript lädt die VirtualBox Guest Additions-Version 7.0.14 herunter, montiert das ISO-Image, führt die Installation aus und entfernt das ISO-Image.

# Lade die VirtualBox Guest Additions-Version 7.0.14 herunter und speichere sie im Verzeichnis /tmp
wget http://download.virtualbox.org/virtualbox/7.0.14/VBoxGuestAdditions_7.0.14.iso -P /tmp

# Mounte das ISO-Image auf das Verzeichnis /mnt
sudo mount -o loop -t iso9660 /tmp/VBoxGuestAdditions_7.0.14.iso /mnt

# Führe die Installation der VBoxLinuxAdditions aus, die sich im gemounteten Verzeichnis befindet
sudo /mnt/VBoxLinuxAdditions.run
sh /mnt/VBoxLinuxAdditions.run
# Unmounte das ISO-Image von /mnt
sudo umount /mnt

# Entferne das ISO-Image aus dem Verzeichnis /tmp
rm /tmp/VBoxGuestAdditions_7.0.14.iso
