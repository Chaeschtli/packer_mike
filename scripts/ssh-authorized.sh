#!/bin/bash -x

# Setze die Berechtigungen für das .ssh-Verzeichnis
chmod 0700 ~/.ssh

# Entferne vorhandene authorized_keys-Datei (falls vorhanden)
mkdir -p ~/.ssh/authorized_keys

# Lade die Vagrant-Pub-Schlüssel herunter und füge sie zur authorized_keys-Datei hinzu
curl -o ~/.ssh/authorized_keys https://raw.githubusercontent.com/hashicorp/vagrant/main/keys/vagrant.pub

# Setze die Berechtigungen für die authorized_keys-Datei
chmod 0600 ~/.ssh/authorized_keys
